package com.mbarskii.aspectj.controller;

import com.mbarskii.aspectj.dto.Job;
import com.mbarskii.aspectj.dto.User;
import com.mbarskii.aspectj.service.PrototypeService;
import com.mbarskii.aspectj.service.UserService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "/v1/users")
public class UserController {

    private final PrototypeService prototypeService;
    private final UserService userService;

    public UserController(PrototypeService prototypeService, UserService userService) {
        this.prototypeService = prototypeService;
        this.userService = userService;
    }

    @GetMapping
    public List<User> getUsers() {
        new Job("persik").getName();
        return getUsersInternal();
    }

    @GetMapping("/class")
    public String getClassName() {
        return prototypeService.showClassName();
    }

    private List<User> getUsersInternal() {
        return userService.getAllUsers();
    }
}
