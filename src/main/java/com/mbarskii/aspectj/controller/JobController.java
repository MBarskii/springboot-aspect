package com.mbarskii.aspectj.controller;

import com.mbarskii.aspectj.service.PrototypeService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/v1/jobs")
public class JobController {

    private final PrototypeService prototypeService;

    public JobController(PrototypeService prototypeService) {
        this.prototypeService = prototypeService;
    }

    @GetMapping("/class")
    public String getClassName() {
        return prototypeService.showClassName();
    }

}
