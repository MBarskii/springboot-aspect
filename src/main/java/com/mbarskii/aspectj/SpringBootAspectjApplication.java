package com.mbarskii.aspectj;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringBootAspectjApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringBootAspectjApplication.class, args);
	}

}
