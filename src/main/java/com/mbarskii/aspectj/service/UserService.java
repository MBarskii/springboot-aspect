package com.mbarskii.aspectj.service;

import com.mbarskii.aspectj.dto.User;

import java.util.List;

public interface UserService {

    List<User> getAllUsers();
}
