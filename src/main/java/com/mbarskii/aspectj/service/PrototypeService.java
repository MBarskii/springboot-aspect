package com.mbarskii.aspectj.service;

public interface PrototypeService {

    String showClassName();
}
