package com.mbarskii.aspectj.service.impl;

import com.mbarskii.aspectj.dto.User;
import com.mbarskii.aspectj.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Service
public class UserServiceImpl implements UserService {

    private final Logger logger = LoggerFactory.getLogger(UserServiceImpl.class);

    @Override
    public List<User> getAllUsers() {
        return getMockUsers();
    }

    private List<User> getMockUsers() {
        logger.info("Generating all the mock users!");
        return IntStream.range(0, 10).mapToObj(i -> new User(i,
                UUID.randomUUID().toString(), UUID.randomUUID().toString()))
                .collect(Collectors.toList());
    }
}