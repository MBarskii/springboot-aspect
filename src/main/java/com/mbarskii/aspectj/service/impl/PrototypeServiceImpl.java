package com.mbarskii.aspectj.service.impl;

import com.mbarskii.aspectj.service.PrototypeService;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Service;

@Service
@Scope(value="prototype", proxyMode= ScopedProxyMode.TARGET_CLASS)
public class PrototypeServiceImpl implements PrototypeService {



    @Override
    public String showClassName() {
        return this.toString();
    }
}
