package com.mbarskii.aspectj.aspect;

import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;

import java.time.ZoneOffset;
import java.time.ZonedDateTime;

@Aspect
public class LoggingInterceptor {

    @Before(value = "execution(* com.mbarskii.aspectj.controller.UserController.getUsersInternal(..))")
    public void addCommandDetailsToMessage() throws Throwable {
        System.out.println("Private method from Spring bean was called at "
                + ZonedDateTime.now((ZoneOffset.UTC)));
    }

    @Before(value = "execution(* com.mbarskii.aspectj.dto.Job.getName(..))")
    public void getNameWasCalled() throws Throwable {
        System.out.println("Method from not Spring bean class was called at  "
                + ZonedDateTime.now((ZoneOffset.UTC)));
    }

    @Before(value = "execution(* com.mbarskii.aspectj.service.impl.PrototypeServiceImpl.showClassName(..))")
    public void methodFromPrototypeBean() throws Throwable {
        System.out.println("Method from Prototype bean was called at  "
                + ZonedDateTime.now((ZoneOffset.UTC)));
    }
}
